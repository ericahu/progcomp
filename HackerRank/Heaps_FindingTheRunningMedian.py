# HEAPS: FINDING THE RUNNING MEDIAN [Difficulty: Hard]
# https://www.hackerrank.com/challenges/ctci-find-the-running-median

# The median of a dataset of integers is the midpoint value of the dataset for which an equal number of integers are less than and greater than the value. To find the median, you must first sort your dataset of integers in non-decreasing order, then:
#	- If your dataset contains an odd number of elements, the median is the middle element of the sorted sample. In the sorted dataset {1,2,3}, 2 is the median.
#	- If your dataset contains an even number of elements, the median is the average of the two middle elements of the sorted sample. In the sorted dataset {1,2,3,4}, (2+3)/2 = 2.5 is the median.
# Given an input stream n of integers, you must perform the following task for each i-th integer:
#	- Add the i-th integer to a running list of integers.
#	- Find the median of the updated list (i.e., for the first element through the i-th element).
#	- Print the list's updated median on a new line. The printed value must be a double-precision number scaled to 1 decimal place (i.e., 12.3 format).


# SOLUTION:

# This is my modified version of a binary search for inserting in the "heap", where I return a list[min, index, max] for O(log n) insertion
def binary_search(list, obj):
	min = 0
	max = len(list) - 1

	while True:
		if max < min:
			return [min, -1, max]
		m = (min + max) // 2	# integer division //
		if list[m] > obj:
			max = m - 1
		elif list[m] < obj:
			min = m + 1
		else:
			return [min, m, max]

if __name__ == "__main__":
	N = int(input())
	heap = []
	for n in range(0,N):
		a = float(input())
		if len(heap) == 0:
			heap.append(a)
			print(a)
			continue

		pos = binary_search(heap, a)
		if pos[1] == -1:
			heap.insert(pos[0], a)
		else:
			heap.insert(pos[1], a)

		# Find median
		l = len(heap)
		if l%2 == 0:
			print((heap[int(l/2)] + heap[int(l/2-1)]) / 2)
		else:
			print(heap[int(l/2)])
