# Arrays: Left Rotation [Difficulty: Easy]
# https://www.hackerrank.com/challenges/ctci-array-left-rotation

# A left rotation on an array of size n shifts each of the array's elements 1 unit to the left. For example, if 2 rotations are performed on array [1,2,3,4,5], then the array would be come [3,4,5,1,2].
# Given an array of n integers, anda number, d, perform d lect rotations on the array. Then print the updated array as a single line of space-separated integers

if __name__ == "__main__":
	n, d = input().split()
	n, d = int(n), int(d)
	numbers = list(map(int, input().split()))
	for i in range(0,d):
		a = numbers.pop(0)
		numbers.append(a)
		
	print(" ".join(str(x) for x in numbers))
