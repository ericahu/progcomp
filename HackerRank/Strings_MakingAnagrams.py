# STRINGS: MAKING ANAGRAMS [Difficulty: Easy]
# https://www.hackerrank.com/challenges/ctci-making-anagrams

# Alice is taking a cryptography class and finding anagrams to be very useful. We consider two strings to be anagrams of each other if the first string's letters can be arranged to form the second string. In other words, both strings must contain the esame exat letters in the same exact frequency. For example, 'bacdc' and 'dcbac' are anagrams, but 'bacdc' and 'dcbad' are not.
# Alice decides on an encryption scheme involving two large strings where encryption is dependent on the minimum number of character deletions required to make the two strings anagrams. Can you help her find this number?
# Given two strings, a and b, that may or may not be of the same length, determine the minimum number of character deletions required to make a and b anagrams. Any characters can be deleted from either of the strings.

A = sorted(input().lower())
B = sorted(input().lower())

count, i = 0,0
while i < min(len(A), len(B)):
	if A[i] < B[i]:
		count += 1
		A.pop(i)
	elif A[i] > B[i]:
		count += 1
		B.pop(i)
	else:
		i += 1

count += abs(len(A) - len(B))
print(count)
