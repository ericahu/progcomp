# INCOMPLETE - TOO SLOW

# TRIES: CONTACTS [Difficulty: Hard]
# https://www.hackerrank.com/challenges/ctci-contacts

# We're going to make our own Contacts application! The application must perform two types of operations:
#	1) add name, where 'name' is a string denoting a contact name. This must store 'name' as a new contact in the application.
#	2) find partial, where 'partial' is a string denoting a partial name to search the application for. It must count the number of contacts starting with 'partial' and print the count on a new line.
# Given n sequential add and find operations, perform each operation in order.

import re, string
from collections import defaultdict

if __name__ == "__main__":
	N = int(input())
	contacts = defaultdict(int)

	for n in range(0,N):
		line = input().split()
		word = line[1]
		if line[0] == "add":
			contacts[word] += 1
		else:
			count = 0
			for w in contacts:
				if line[1] in w:
					count += contacts[w]
			print(count)
