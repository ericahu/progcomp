# HASH TABLES: RANSOM NOTE [Difficulty: Easy]
# https://www.hackerrank.com/challenges/ctci-ransom-note

# A kidnapper wrote a ransom note but is worried it will be traced back to him. He found a magazine and wants to know if he can cut out whole words from it and use them to create an untraceable replica of his ransom note. The words in his note are case-sensitive and he must use whole words available in the magazine, meaning he cannot use substrings or concatenation to create the words he needs.
# Given the words in the magazine and the words in the ransom note, print Yes if he can replicate his ransom note exactly using whole words from the magazine; otherwise, print No.

def solve():
	m, n = map(int, input().split())
	if n > m:
		return("No")
	line_m = sorted(list(input().split()))
	line_n = sorted(list(input().split()))

	i = 0
	while i < min(len(line_m), len(line_n)):
		if line_m[i] < line_n[i]:
			line_m.pop(i)
		elif line_m[i] == line_n[i]:
			i += 1
		else:
			return("No")
	return("Yes")


if __name__ == "__main__":
	print(solve())
